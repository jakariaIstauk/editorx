<?php
/**
 * Plugin Name: EditorX
 * Description: Edit your post from front end. 
 * Author: Jakaria Istauk
 * Version: 1.0.0
 * Author URI: https://profiles.wordpress.org/jakariaistauk/#content-plugins
 * Text Domain: editorx
 * Domain Path: /languages
 *
 */

namespace Jakaria\EditorX;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Main class for the plugin
 * @package Plugin
 * @author Jakaria <jakariamd35@gmail.com>
 */
final class Plugin {
    
    public static $_instance;

    public function __construct() {
        $this->include();
        $this->define();
        $this->hook();
    }

    /**
     * Includes files
     */
    public function include() {
        require_once( dirname( __FILE__ ) . '/vendor/autoload.php' );
    }

    /**
     * Define variables and constants
     */
    public function define() {
        // constants
        define( 'EDITORX', __FILE__ );
        define( 'EDITORX_DIR', dirname( EDITORX ) );
        define( 'EDITORX_ASSETS', plugins_url( 'assets', EDITORX ) );
    }

    /**
     * Hooks
     */
    public function hook() {

        if ( is_admin() ):
            $admin = new Admin;
            add_action( 'plugins_loaded', [ $admin, 'i18n'] );
        
        else:
            $front = new Front;
            add_action( 'wp_enqueue_scripts', [ $front, 'enqueue_scripts' ] );
            add_action( 'wp_head', [ $front, 'head' ] );
            add_action( 'wp_footer', [ $front, 'footer' ] );
            add_action( 'the_title', [ $front, 'title' ], 10, 2 );
            add_action( 'the_excerpt', [ $front, 'excerpt' ] );
            add_filter( 'the_content', [ $front, 'content'] );
        endif;

        $ajax = new Ajax;
        add_action( 'wp_ajax_editorx-edit-title', [ $ajax, 'edit_title' ] );
        add_action( 'wp_ajax_editorx-edit-content', [ $ajax, 'edit_content' ] );

    }
 
    /**
     * Cloning is forbidden.
     */
    public function __clone() { }

    /**
     * Unserializing instances of this class is forbidden.
     */
    public function __wakeup() { }

    /**
     * Instantiate the plugin
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}

Plugin::instance();