<?php
namespace Jakaria\EditorX;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package Plugin
 * @subpackage Front
 * @author Jakaria Istauk <jakariamd35@gmail.com>
 */
class Front{
    public function enqueue_scripts() {
        wp_enqueue_style( 'editorx', plugins_url( "assets/css/front.css", EDITORX ), '1.0.0' );
        wp_enqueue_script( 'editorx', plugins_url( "assets/js/front.js", EDITORX ), [ 'jquery' ] );
        $localized = [
            'ajaxurl'   => admin_url( 'admin-ajax.php' ),
            '_wpnonce'   => wp_create_nonce()
        ];
        wp_localize_script( 'editorx', 'EDITORX', apply_filters( "editorx-localized", $localized ) );
    }

    public function head() {
        
    }

    public function footer() {

        if ( ! current_user_can( 'edit_posts' ) ) return;
        echo "<div id='editorx-notification'></div>";

        $status = isset( $_COOKIE['editorx_editing'] ) && $_COOKIE['editorx_editing'] == 'enabled' ? 'disable' : 'enable';
        $label = $status == 'enable' ? __( 'Enable EditorX', 'editorx' ) : __( 'Disable EditorX', 'editorx' );
        echo "<button id='editorx-enabling-btn' class='" .  esc_attr( $status ) . "-editorx'>" . esc_html(  $label  ) . "</button>";
    }

    public function title( $title, $id ) {

        if (
            !current_user_can( 'edit_post', $id ) || 
            get_post_type( $id ) != "post" || 
            ! isset( $_COOKIE['editorx_editing'] ) || 
            $_COOKIE['editorx_editing'] != 'enabled' 
        ) return $title;

        $permalink = get_the_permalink( $id ); 
        return "
        <span contenteditable='true' class='editorx-edit-title' data-post='{$id}'>{$title}</span>
        <span class='editorx-old-title' >{$title}</span>
        ";
    }

    public function excerpt( $excerpt ) {

        global $post;
        if (
            !current_user_can( 'edit_post', $post->ID ) || 
            get_post_type( $post->ID ) != "post" || 
            ! isset( $_COOKIE['editorx_editing'] ) || 
            $_COOKIE['editorx_editing'] != 'enabled' ||
            ! has_excerpt( $post->ID )
        ) return $excerpt;

        $permalink = get_the_permalink( $post->ID ); 
        return "
        <span contenteditable='true' class='editorx-edit-content' data-type='excerpt' data-post='{$post->ID}'>{$excerpt}</span>
        <span class='editorx-old-content' >{$excerpt}</span>
        ";
    }

    public function content( $content ) {

        global $post;
        if (
            !current_user_can( 'edit_post', $post->ID ) || 
            get_post_type( $post->ID ) != "post" || 
            ! isset( $_COOKIE['editorx_editing'] ) || 
            $_COOKIE['editorx_editing'] != 'enabled' 
        ) return $content;

        $permalink = get_the_permalink( $post->ID ); 
        return "
        <span contenteditable='true' class='editorx-edit-content' data-type='content' data-post='{$post->ID}'>{$content}</span>
        <span class='editorx-old-content' >{$content}</span>
        ";
    }
}