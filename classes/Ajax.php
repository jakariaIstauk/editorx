<?php
namespace Jakaria\EditorX;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package Plugin
 * @subpackage Front
 * @author Jakaria Istauk <jakariamd35@gmail.com>
 */
class Ajax{
    public function edit_title() {

        $response = [ 'status' => 0, 'message' => __( 'Unauthorized', 'editorx' ) ];

        if( ! wp_verify_nonce( $_POST['_wpnonce'] ) ){
            wp_send_json( $response );
        }
        if ( ! isset( $_POST['post_id'] ) || $_POST['post_id'] == '' ) {
            $response['message'] = __( 'Invalid Post ID', 'editorx' );
            wp_send_json( $response );
        }

        $post_data = [ 
            'ID'         => sanitize_text_field( $_POST['post_id'] ),
            'post_title' =>  wp_kses_post( $_POST['title'] )

        ];

        $post_updated = wp_update_post( $post_data );
        if ( ! $post_updated ) {
            $response['message'] = __( 'Something went wrong', 'editorx' );
            wp_send_json( $response );
        }

        $response['status'] = 1;
        $response['message'] = __( 'Post Title Updated', 'editorx' );
        wp_send_json( $response );
    }

    public function edit_content() {

        $response = [ 'status' => 0, 'message' => __( 'Unauthorized', 'editorx' ) ];

        if( ! wp_verify_nonce( $_POST['_wpnonce'] ) ){
            wp_send_json( $response );
        }
        if ( ! isset( $_POST['post_id'] ) || $_POST['post_id'] == '' ) {
            $response['message'] = __( 'Invalid Post ID', 'editorx' );
            wp_send_json( $response );
        }

        $post_data = [  'ID' => sanitize_text_field( $_POST['post_id'] ) ];

        if ( isset( $_POST['type'] ) && $_POST['type'] == 'excerpt' ) {
            $post_data['post_excerpt'] = wp_kses_post( $_POST['content'] );
        }
        else if ( isset( $_POST['type'] ) && $_POST['type'] == 'content' ) {
            $post_data['post_content'] = wp_kses_post( $_POST['content'] );
        }
        else{
            $response['message'] = __( 'Invalid Content', 'editorx' );
            wp_send_json( $response );
        }
        
        $post_updated = wp_update_post( $post_data );
        if ( ! $post_updated ) {
            $response['message'] = __( 'Something went wrong', 'editorx' );
            wp_send_json( $response );
        }

        $response['status'] = 1;
        $response['message'] = __( 'Post Content Updated', 'editorx' );
        wp_send_json( $response );
    }
}