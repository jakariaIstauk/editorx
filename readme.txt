=== EditoX ===
Contributors: jakariaistauk, al-imran-akash
Tags: Wordpress, Editor, Post Editor, Customizer
Requires at least: 4.0
Tested up to: 5.9
Stable tag: 1.0.0
Requires PHP: 5.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Edit your post title, excerpt and description from front page

== Installation ==

1. Install Editorx using the native Plugin installer, or download the zip and extract it in the wp-content/plugins/ directory.
2. Activate the plugin through the ‘Plugins’ 3. 
3. You get a enable/disable floating button on the front page.
4. When you enable it the title, excerpt (if has), and the content will be editable

== Frequently Asked Questions ==

= Which posts are editable? =
Currently, you can edit only blog posts. 
= Does it change the product name? =
No, but we will add the capability ver soon 

== Screenshots ==
1. Enable/Disable Floating button
2. After enable
3. After edit from front page

== Changelog ==
= v1.0.0 - 2022-02-25 =
* Initial release
