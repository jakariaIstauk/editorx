function editorx_setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function editorx_getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function editorx_eraseCookie(name) {   
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

jQuery(function($){
	var permalinks = $('.editorx-edit-title').closest('a');
	permalinks.removeAttr('href');

	function editorx_notification(message) {
		$('#editorx-notification').fadeOut().html(message).fadeIn();
		setTimeout(function() {
			$('#editorx-notification').fadeOut();
		}, 1500);
	}

	$(document).on('click','#editorx-enabling-btn',function(e){
		e.preventDefault();
		if ( $(this).hasClass('enable-editorx') ) {
			editorx_setCookie('editorx_editing', 'enabled',2);
			window.location.href='';
		}
		if ( $(this).hasClass('disable-editorx') ) {			
			editorx_eraseCookie('editorx_editing');
			window.location.href='';
		}
	});

	$(document).on('focusout', '.editorx-edit-title', function(e) {
		var title = $(this).html();
		var post_id = $(this).data('post');
		var parent = $(this).closest('a');
		var _title = $('.editorx-old-title', parent ).html();

		if( title == _title ) return;

		$('.editorx-old-title', parent ).html(title);

		$.ajax({
			url: EDITORX.ajaxurl,
			data: { action: 'editorx-edit-title', title: title, post_id:post_id, _wpnonce: EDITORX._wpnonce },
			type: 'post',
			success: function(res){
				console.log(res)
				if ( res.status == 1 ) {
					editorx_notification(res.message);
				}
			},
			error: function(res) {
				console.log(res)
			}
		});
	});

	$('.editorx-edit-content .read-more, .editorx-old-content .read-more').remove();

	$(document).on('focusout', '.editorx-edit-content', function(e) {
		var content = $(this).html();
		var post_id = $(this).data('post');
		var type 	= $(this).data('type');
		var parent 	= $(this).closest('.entry-content');
		var _content = $('.editorx-old-content', parent ).html();

		if( content == _content ) return;

		$('.editorx-old-content', parent ).html(content);

		$.ajax({
			url: EDITORX.ajaxurl,
			data: { action: 'editorx-edit-content', content: content, type: type, post_id:post_id, _wpnonce: EDITORX._wpnonce },
			type: 'post',
			success: function(res){
				console.log(res)
				if ( res.status == 1 ) {
					editorx_notification(res.message);
				}
			},
			error: function(res) {
				console.log(res)
			}
		});
	});
});